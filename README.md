```docker-compose up .```
or
```LOG_FILE_PATH=/tmp/mylogfilepath.log docker-compose up . --build```
(without `--build` the file path from shell will not be used, this is the odd element in templating).

watch logs:
```docker exec -it masquerade-web-1 tail -f {logfilepath}```
(default filepath is `/tmp/masquerade.log`)

try the endpoints:

call root endpoint:
```curl localhost:3333/```

call hello endpoint:
```curl localhost:3333/hello```

return number of calls to hello endpoint:
```curl localhost:3333/logs?endpoint=hello --output -```
(the `--output -` enables displaying binaries in terminals, at least it does so in zsh/arch/alacritty)

show number of all calls - including this one:
```curl localhost:3333/logs/preview```
