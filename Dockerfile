FROM golang:1.20.5-alpine

WORKDIR /go/src/app

ADD src src
WORKDIR /go/src/app/src
# build the app binary so that it can be used later
RUN sh -c "go build ./entrypoint/main.go"
