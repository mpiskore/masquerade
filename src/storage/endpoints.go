package storage

import (
  "fmt"
  "errors"
)

type StorageEndpoint int64

const (
	Root StorageEndpoint = iota
	Hello
	Extra
	Logs
	Preview
)

func StringToStorageEndpoint(endpointName string) (StorageEndpoint, error) {
	switch endpointName {
	case "root":
		return Root, nil
	case "hello":
		return Hello, nil
	case "extra":
		return Extra, nil
	case "logs":
		return Logs, nil
	case "preview":
		return Preview, nil
	}
  return 0, errors.New(fmt.Sprintf("Storage endpoint undefined: %s", endpointName))
}

func StorageEndpointToString(endpoint StorageEndpoint) (string, error) {
  switch endpoint {
  case Root:
    return "root", nil
  case Hello:
    return "hello", nil
  case Extra:
    return "extra", nil
  case Logs:
    return "logs", nil
  case Preview:
    return "preview", nil
  }
  return "", errors.New(fmt.Sprintf("Storage endpoint undefined: %d", endpoint))
}
