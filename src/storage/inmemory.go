package storage

import (
	"fmt"
	"os"
	"sort"
)

// "global" storage in memory, initialized with app
type InMemoryStorage struct {
	Content map[StorageEndpoint]int64 // maintains the count of each endpoint called
}

func (s *InMemoryStorage) Init() {
	s.Content = make(map[StorageEndpoint]int64)
}

// Add 1 to the count of endpoints
func (s *InMemoryStorage) Input(endpoint StorageEndpoint) {
	if value, ok := s.Content[endpoint]; ok {
		s.Content[endpoint] = value + 1
	} else {
		s.Content[endpoint] = 1
	}
}

// Returns the storage sorted by keys
// FIXME: this is just a map and thus it doesn't keep the order anyways.
func (s *InMemoryStorage) Preview() map[StorageEndpoint]int64 {
  var sortedKeys = []string{}
  for k := range s.Content {
    value, err := StorageEndpointToString(k)
    if err != nil {
      panic(err)
    }
    sortedKeys = append(sortedKeys, value)
  }
  sort.Strings(sortedKeys)
  response := make(map[StorageEndpoint]int64)
  for _, keyString := range sortedKeys {
    key, err := StringToStorageEndpoint(keyString)
    if err != nil {
      panic(err)
    }
    response[key] = s.Content[key]
  }
  return response
}

func (s *InMemoryStorage) Get(endpoint StorageEndpoint) int64 {
	if value, ok := s.Content[endpoint]; ok {
		return value
	} else {
		return int64(0)
	}
}

func Init() *InMemoryStorage {
	fmt.Println("Initializing the in memory storage...")
	storage := InMemoryStorage{}
	storage.Init()
	return &storage
}

func BrowseLogs() (*string, error) {
	content, err := os.ReadFile(os.Getenv("LOG_FILE_PATH"))
	if err != nil {
		return nil, err
	}
	stringContent := string(content)
	return &stringContent, nil
}
