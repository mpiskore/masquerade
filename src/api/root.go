package api

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/mpiskore/masquerade/loggers"
	"gitlab.com/mpiskore/masquerade/storage"
)

type API struct {
  S *storage.InMemoryStorage
  Log loggers.Logger
}

func Init() API {
  api := API{}
  // type of storage should be defined in config file / godotenv settings
  api.S = storage.Init()
  api.Log = loggers.Init()
  return api
}

// API will serve all the defined endpoints and pass requests to particular handlers.
// We still need to determine a proper framework for this - or not use one at all.
func (api *API) Handle(){
  // Echo endpoints
  http.HandleFunc("/", api.handleRoot)
  http.HandleFunc("/hello", api.handleHello)
  http.HandleFunc("/extra", api.handleExtra)
  // Endpoints showing the number of requests
  http.HandleFunc("/logs", api.handleGetLogs)
  http.HandleFunc("/logs/preview", api.handlePreviewLogs)

  fmt.Println("Server started...")
  err := http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), nil)
  if err != nil {
    fmt.Printf("Error while serving HTTP: %s\n", err)
    os.Exit(0) // quit gracefully after showing the error.
  }
}

func (api *API) handleRoot(w http.ResponseWriter, r *http.Request) {
  api.Log.Info("Called root endpoint")
  api.S.Input(storage.Root)
  w.Write([]byte(r.URL.Path))
}

func (api *API) handleHello(w http.ResponseWriter, r *http.Request) {
  api.Log.Info("Called hello endpoint")
  api.S.Input(storage.Hello)
  w.Write([]byte(r.URL.Path))
}

func (api *API) handleExtra(w http.ResponseWriter, r *http.Request) {
  api.Log.Info("Called extra endpoint")
  api.S.Input(storage.Extra)
  w.Write([]byte(r.URL.Path))
}

func (api *API) handleGetLogs(w http.ResponseWriter, r *http.Request) {
  api.S.Input(storage.Logs)
  endpoint := r.URL.Query().Get("endpoint")
  storageEndpoint, err := storage.StringToStorageEndpoint(endpoint)
  if err != nil {
    api.Log.Error(fmt.Sprintf("Failed calling get logs endpoint with endpoint \"%s\"", endpoint))
    w.Write([]byte(fmt.Sprintf("No such endpoint \"%s\"", endpoint)))
    return
  }
  err = api.Log.Warning(fmt.Sprintf("Called get logs endpoint \"%s\"", endpoint))
  if err != nil {
    w.Write([]byte(err.Error()))
  }
  w.Write([]byte(fmt.Sprintf("%d", api.S.Get(storageEndpoint))))
}

func (api *API) handlePreviewLogs(w http.ResponseWriter, r *http.Request) {
  api.Log.Warning("Called preview logs endpoint")
  api.S.Input(storage.Preview)
  preview := api.S.Preview()
  for key, value := range(preview) {
    endpointName, err := storage.StorageEndpointToString(key)
    if err != nil {
      // This is very unlikely as the values inputted into storage are taken from endpoint definitions
      w.Write([]byte(fmt.Sprintf("An unexpected error occurred: %s", err.Error())))
      return
    }
    w.Write([]byte(fmt.Sprintf("%s: %d\n", endpointName, value)))
  }
}
