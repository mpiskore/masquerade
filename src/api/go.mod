module gitlab.com/mpiskore/masquerade/api

go 1.20

require (
	gitlab.com/mpiskore/masquerade/loggers v0.0.0-20231216143355-7da97958f42e
	gitlab.com/mpiskore/masquerade/storage v0.0.0-20231216143355-7da97958f42e
)
