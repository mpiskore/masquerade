module gitlab.com/mpiskore/masquerade/entrypoint

go 1.20

require (
	github.com/joho/godotenv v1.5.1
	gitlab.com/mpiskore/masquerade/api v0.0.0-20231216143355-7da97958f42e
)

require (
	gitlab.com/mpiskore/masquerade/loggers v0.0.0-20231216143355-7da97958f42e // indirect
	gitlab.com/mpiskore/masquerade/storage v0.0.0-20231216143355-7da97958f42e // indirect
)
