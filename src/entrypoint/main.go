package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/mpiskore/masquerade/api"
)

const TEMPLATE_CONFIG_FILENAME = ".env.template"
const DEFAULT_CONFIG_FILENAME = ".env"

// main function is the main entrypoint to our system. It will be used for running the services
// until we can move towards a more complex architecture.
func main() {
	LoadConfig()
	api := api.Init()
	api.Handle()
}

// The config loader lives here now, I believe that there cannot be another file in the entrypoint dir (or package main) that is used here because of the build command being used in dockerfile.
func LoadConfig() {
	err := godotenv.Load(DEFAULT_CONFIG_FILENAME)
	if err != nil {
		fmt.Println("No config file with name", DEFAULT_CONFIG_FILENAME, "found, falling back to", TEMPLATE_CONFIG_FILENAME, ".")
		err = godotenv.Load(TEMPLATE_CONFIG_FILENAME)
		if err != nil {
			fmt.Println("No template config file with name", DEFAULT_CONFIG_FILENAME, "found, exiting.")
			panic(err)
		}
	}
}
