package loggers

import (
	"fmt"
	"os"
	"time"
)

type Logger struct {
  filePath string
}

type LogLevel int64

const (
  Info LogLevel = iota
  Warning
  Error
)

// All the strings have constant length 7 to ensure smooth logging format.
func (l LogLevel) String() string {
  switch l {
  case Info:
    return "INFO   "
  case Warning:
    return "WARNING"
  case Error:
    return "ERROR  "
  }
  return "unknown"
}

func Init() Logger {
  filePath := os.Getenv("LOG_FILE_PATH")
  fmt.Println("The filepath is", filePath)
  return Logger{filePath: filePath}
}

func (l *Logger) Log(message, level string) error {
  timestamp := time.Now().Format(time.RFC3339)
  log := fmt.Sprintf("%s | %s | %s", timestamp, level, message)
  existingContent, err := os.ReadFile(l.filePath)
  if err != nil {
    return err
  }
  formattedContent := []byte(log + "\n")
  newContent := append(existingContent, formattedContent...)
  os.WriteFile(l.filePath, newContent, 0666)
  if err != nil {
    return err
  }
  return nil
}

// Helper functions that will be actually called
func (l *Logger) Info(message string) error {
  return l.Log(message, Info.String())
}

func (l *Logger) Warning(message string) error {
  return l.Log(message, Warning.String())
}

func (l *Logger) Error(message string) error {
  return l.Log(message, Error.String())
}
