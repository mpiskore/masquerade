#!/usr/bin/python

import subprocess
import requests
import time
from requests.models import HTTPError

REPO_PATH = "/home/marek/codes/projects/masquerade"
BASH_EXEC = "/bin/bash"

def test_func(func):
    def wrap(*args, **kwargs):
        runner: Runner = args[0]
        runner.run_app()
        time.sleep(1)
        try:
            func(*args, **kwargs)
            print(f"TEST PASSED: {func.__name__}")
        except AssertionError as e:
            print(f"TEST FAILED: {func.__name__}")
            print(e)
        runner.kill_app()
    return wrap

class Runner:
    def __init__(self, log_file_path: str|None = None):
        self.log_file_path = log_file_path or "/tmp/masquerade.log"

    def run_app(self):
        # TOOD: consider persisting the containers between tests but maybe purge the storage?
        # run the application through docker-compose
        subprocess.check_output(f"cd {REPO_PATH}", shell=True, executable=BASH_EXEC)
        try:
            subprocess.check_output(f"LOG_FILE_PATH={self.log_file_path} docker-compose up --build -d", shell=True, executable=BASH_EXEC)
        except subprocess.CalledProcessError:
            print("Server start failed. Please ensure no other server works.")

    def kill_app(self):
        subprocess.check_output(f"cd {REPO_PATH}", shell=True, executable=BASH_EXEC)
        subprocess.check_output("docker-compose down", shell=True, executable=BASH_EXEC)

    def call_endpoint(self, path: str) -> bytes:
        response = requests.get(f"http://localhost:3333{path}")
        if response.status_code != 200:
            raise HTTPError(response.content)
        return response.content

@test_func
def test_log_and_read(runner: Runner):
    response = runner.call_endpoint("/")
    assert response == b"/", response

    response = runner.call_endpoint("/logs/preview")
    assert response == b"preview: 1\nroot: 1\n", response

@test_func
def test_multiple_log_and_read(runner: Runner):
    for _ in range (3):
        response = runner.call_endpoint("/")
        assert response == b"/", response

    response = runner.call_endpoint("/logs/preview")
    assert response == b"preview: 1\nroot: 3\n", response

@test_func
def test_multiple_endpoints_log_and_read(runner: Runner):
    for _ in range (3):
        response = runner.call_endpoint("/")
        assert response == b"/", response
    for _ in range (2):
        response = runner.call_endpoint("/hello")
        assert response == b"/hello", response
    for _ in range (4):
        # This falls back to root and even though it will return the same text (echo)
        # It will be logged as `root`
        response = runner.call_endpoint("/weird")
        assert response == b"/weird", response

    response = runner.call_endpoint("/logs/preview")
    assert response == b"hello: 2\npreview: 1\nroot: 7\n", response

#################
#  TEST CASES   #
#################

runner = Runner()
# test_log_and_read(runner)
# test_multiple_log_and_read(runner)
test_multiple_endpoints_log_and_read(runner)
